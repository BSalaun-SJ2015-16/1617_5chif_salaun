package thymian.thymian;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ModelAttribute;


@Controller
@RequestMapping("/")
public class SpringController {
	
	 @RequestMapping("")
	    String index(Model modle){
		 	modle.addAttribute("index",new Index());
	        return "index";
	    }

	 @RequestMapping("auswerten")
	    String auswerten(Model modle){
		 	modle.addAttribute("auswerten");
	        return "auswerten";
	    }
	 
	    public String indexSubmit(@ModelAttribute Index index) {
	        return "result";
	    }
}